import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PscFormPage } from './psc-form';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    PscFormPage,
  ],
  imports: [
    IonicPageModule.forChild(PscFormPage),
    FormsModule,
  ],
  exports: [
    PscFormPage
  ]
})
export class PscFormPageModule {}
