import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BumilFormPage } from './bumil-form';

@NgModule({
  declarations: [
    BumilFormPage,
  ],
  imports: [
    IonicPageModule.forChild(BumilFormPage),
  ],
  exports: [
    BumilFormPage
  ]
})
export class BumilFormPageModule {}
