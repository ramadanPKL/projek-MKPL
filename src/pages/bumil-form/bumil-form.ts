import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
// import { BumilForm2Page } from '../bumil-form2/bumil-form2';

/**
 * Generated class for the BumilFormPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-bumil-form',
  templateUrl: 'bumil-form.html',
})
export class BumilFormPage {

  value: any;

  currentUser = JSON.parse(localStorage.getItem('currentUser'));

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    // deepcopying object
    this.value = Object.assign({}, navParams.get('value'));
    this.value.idpuskesmas = this.currentUser.data.idpuskesmas;
  }

  nextForm() {
    this.navCtrl.push("BumilForm2Page", {
      value: this.value
    });
  }

  disableCheck() {
    if (this.value.tanggal != '' && this.value.puskesmas != '' && this.value.idkelurahan != '' && this.value.tempat_rencana_persalinan != '' && this.value.idrencana_persalinan_biaya != '' && this.value.idrencana_persalinan_kendaraan != '' && this.value.idketerangan_bpjs != '') {
      return false;
    } else {
      return true;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BumilFormPage');
  }

}
