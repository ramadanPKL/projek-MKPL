import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GiziburukSearchPage } from './giziburuk-search';

@NgModule({
  declarations: [
    GiziburukSearchPage,
  ],
  imports: [
    IonicPageModule.forChild(GiziburukSearchPage),
  ],
  exports: [
    GiziburukSearchPage
  ]
})
export class GiziburukSearchPageModule {}
