import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TbFormPage } from './tb-form';

@NgModule({
  declarations: [
    TbFormPage,
  ],
  imports: [
    IonicPageModule.forChild(TbFormPage),
  ],
  exports: [
    TbFormPage
  ]
})
export class TbFormPageModule {}
