import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
// import {TbForm1Page} from '../tb-form1/tb-form1';

@IonicPage()
@Component({
  selector: 'page-tb-form',
  templateUrl: 'tb-form.html',
})
export class TbFormPage {

  value: any;
  currentUser = JSON.parse(localStorage.getItem('currentUser'));

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.value = Object.assign({}, navParams.get('value'));
    this.value.idpuskesmas = this.currentUser.data.idpuskesmas;
    
  }

  nextForm() {
    this.navCtrl.push("TbForm1Page", {
      value: this.value
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TbFormPage');
  }



}
