import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TbForm3Page } from './tb-form3';

@NgModule({
  declarations: [
    TbForm3Page,
  ],
  imports: [
    IonicPageModule.forChild(TbForm3Page),
  ],
  exports: [
    TbForm3Page
  ]
})
export class TbForm3PageModule {}
