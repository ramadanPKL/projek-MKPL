import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { NccServiceProvider } from '../../providers/ncc-service/ncc-service';

/**
 * Generated class for the BumilFormPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-tb-form3',
  templateUrl: 'tb-form3.html',
})
export class TbForm3Page {
  value: any;
  data = new FormData();

  constructor(public navCtrl: NavController, public navParams: NavParams, public nccService: NccServiceProvider, private alertCtrl: AlertController) {
    this.value = navParams.get('value');
  }

  confirmTb() {
    for (let key in this.value) {
      this.data.append(key, this.value[key]);
    }

    if (this.value.iddata_tb) {
      //this.nccService.editTb(this.data).subscribe();
      this.nccService.editTb(this.data).subscribe((res) => {
        let alert = this.alertCtrl.create({
          title: res.success? 'Success':'Failed',
          subTitle: res.message,
          buttons: ['OK']
        });

        alert.present(prompt);
      });

    } else {
      //this.nccService.inputTb(this.data).subscribe();
      this.nccService.inputTb(this.data).subscribe((res) => {
        let alert = this.alertCtrl.create({
          title: res.success? 'Success':'Failed',
          subTitle: res.message,
          buttons: ['OK']
        });

        alert.present(prompt);
      });

    }
    this.navCtrl.setRoot("TbListPage");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TbForm3Page');
  }



}
