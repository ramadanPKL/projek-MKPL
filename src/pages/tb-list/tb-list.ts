import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { NccServiceProvider } from '../../providers/ncc-service/ncc-service';
import { Injectable } from '@angular/core';
import { PopoverController } from 'ionic-angular';
//import { Headers, Http } from '@angular/http';

@IonicPage()
@Component({
  selector: 'page-tb-list',
  templateUrl: 'tb-list.html',
  providers: [NccServiceProvider]
})
export class TbListPage {
  tbList = [];
  amount: number = 15;
  page: number = 1;

  // dataTb: string[];
  errorMessage: string;
  constructor(public navCtrl: NavController, private nccService: NccServiceProvider, public popoverCtrl: PopoverController) {
    this.getTb();
    console.log(this.tbList);
  }

  getTb() {
    this.nccService.getTb(this.amount, this.page).subscribe(data => {
      for(let tb of data.data) {
          this.tbList.push(tb);
        };});
  }

  getDetail(item) {
    this.navCtrl.push("TbDetailPage", {
          item:item
      });
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create("TbPopoverPage");
    popover.present({
      ev: myEvent
    });
  }


  doInfinite(infiniteScroll) {
    console.log('Begin async operation');
    // this.amount+=15;
    this.page+=1;

    setTimeout(() => {
      this.getTb();

      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 1500);

  }


}
