import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TbListPage } from './tb-list';

@NgModule({
  declarations: [
    TbListPage,
  ],
  imports: [
    IonicPageModule.forChild(TbListPage),
  ],
  exports: [
    TbListPage
  ]
})
export class TbListPageModule {}
