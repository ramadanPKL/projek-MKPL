import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Nav, AlertController } from 'ionic-angular';
import { NccServiceProvider } from '../../providers/ncc-service/ncc-service';
// import { BumilFormPage } from '../bumil-form/bumil-form';

/**
 * Generated class for the BumilDetailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-bumil-detail',
  templateUrl: 'bumil-detail.html',
})
export class BumilDetailPage {

  detail: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private nccService: NccServiceProvider, private nav: Nav, private alertCtrl: AlertController) {
    this.detail = navParams.get('item');
  }

  editBumil() {
    this.navCtrl.push("BumilFormPage", {
      value: this.detail
    });
  }

  deleteBumil() {
    let confirm = this.alertCtrl.create({
      title: 'Konfirmasi',
      message: `Apakah anda yakin menghapus <b>${this.detail.nama_ibu}</b> dari data ibu hamil?`,
      buttons: [
        {
          text: 'Batal',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Ok',
          handler: () => {
            console.log('Agree clicked');
            this.nccService.deleteBumil(this.detail.iddata_bumil).subscribe((res) => {
              let alert = this.alertCtrl.create({
                title: res.success? 'Success':'Failed',
                subTitle: res.message,
                buttons: ['OK']
              });

              alert.present(prompt);
            });
            this.nav.setRoot("BumilListPage");
          }
        }
      ]
    });
    confirm.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BumilDetailPage');
  }

}
