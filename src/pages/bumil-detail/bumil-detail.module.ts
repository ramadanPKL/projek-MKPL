import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BumilDetailPage } from './bumil-detail';

@NgModule({
  declarations: [
    BumilDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(BumilDetailPage),
  ],
  exports: [
    BumilDetailPage
  ]
})
export class BumilDetailPageModule {}
