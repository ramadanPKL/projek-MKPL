import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TbForm1Page } from './tb-form1';

@NgModule({
  declarations: [
    TbForm1Page,
  ],
  imports: [
    IonicPageModule.forChild(TbForm1Page),
  ],
  exports: [
    TbForm1Page
  ]
})
export class TbForm1PageModule {}
