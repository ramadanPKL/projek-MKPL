import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
// import {TbForm2Page} from '../tb-form2/tb-form2';
/**
 * Generated class for the BumilFormPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-tb-form1',
  templateUrl: 'tb-form1.html',
})
export class TbForm1Page {
  value: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.value = navParams.get('value');
  }

  nextForm() {
    this.navCtrl.push("TbForm2Page", {
      value: this.value
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TbForm1Page');
  }

}
