import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Nav } from 'ionic-angular';
import { NccServiceProvider } from '../../providers/ncc-service/ncc-service';

/**
 * Generated class for the GiziburukForm2Page page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-giziburuk-form2',
  templateUrl: 'giziburuk-form2.html',
})
export class GiziburukForm2Page {

  balita = [];
  value: any;
  data = new FormData();

  constructor(public navCtrl: NavController, public navParams: NavParams, private nccService: NccServiceProvider, private nav: Nav) {
    this.value = navParams.get('value');
    this.getBalita();
  }

  confirmGiziburuk() {
    // this.value.status_keluarga_nongakin = 1;
    for (let key in this.value) {
      this.data.append(key, this.value[key]);
    }

    console.log(this.value);

    if (this.value.iddata_gizi_buruk) {
      this.nccService.editGiziburuk(this.data).subscribe();
    } else {
      this.nccService.inputGiziburuk(this.data).subscribe();
    }
    this.nav.setRoot("GiziburukListPage");
  }

  getBalita() {
    this.nccService.getBalita().subscribe((data) => {
      this.balita = data.data.sort().reverse();
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GiziburukForm2Page');
  }

}
