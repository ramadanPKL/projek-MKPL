import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GiziburukForm2Page } from './giziburuk-form2';

@NgModule({
  declarations: [
    GiziburukForm2Page,
  ],
  imports: [
    IonicPageModule.forChild(GiziburukForm2Page),
  ],
  exports: [
    GiziburukForm2Page
  ]
})
export class GiziburukForm2PageModule {}
