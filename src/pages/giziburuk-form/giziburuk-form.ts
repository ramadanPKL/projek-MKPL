import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

// import { GiziburukForm2Page } from '../giziburuk-form2/giziburuk-form2';

/**
 * Generated class for the GiziburukFormPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-giziburuk-form',
  templateUrl: 'giziburuk-form.html',
})
export class GiziburukFormPage {

  value: any = {
  };

  currentUser = JSON.parse(localStorage.getItem('currentUser'));

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    //deep copying object
    this.value = Object.assign({}, navParams.get('value'));
    this.value.idpuskesmas = this.currentUser.idpuskesmas;
  }

  nextForm() {
    this.navCtrl.push("GiziburukForm2Page", {
      value: this.value
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GiziburukFormPage');
  }

}
