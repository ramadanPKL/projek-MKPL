import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GiziburukFormPage } from './giziburuk-form';

@NgModule({
  declarations: [
    GiziburukFormPage,
  ],
  imports: [
    IonicPageModule.forChild(GiziburukFormPage),
  ],
  exports: [
    GiziburukFormPage
  ]
})
export class GiziburukFormPageModule {}
