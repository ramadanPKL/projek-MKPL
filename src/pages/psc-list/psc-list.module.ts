import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PscListPage } from './psc-list';

@NgModule({
  declarations: [
    PscListPage,
  ],
  imports: [
    IonicPageModule.forChild(PscListPage),
  ],
  exports: [
    PscListPage
  ]
})
export class PscListPageModule {}
