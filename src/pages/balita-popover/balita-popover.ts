import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, ViewController } from 'ionic-angular';

/**
 * Generated class for the BalitaPopoverPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-balita-popover',
  templateUrl: 'balita-popover.html',
})
export class BalitaPopoverPage {

  balita = {
    nama_balita: '',
    nama_orang_tua: '',
    idjeniskelamin: '',
  }
  constructor(public navCtrl: NavController, public navParams: NavParams, private app: App, private viewCtrl: ViewController) {
  }

  addBalita() {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.app.getRootNav().push('BalitaFormPage', {
      'balita': this.balita,
    });
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BalitaPopoverPage');
  }

}
