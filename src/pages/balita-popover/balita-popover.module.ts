import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BalitaPopoverPage } from './balita-popover';

@NgModule({
  declarations: [
    BalitaPopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(BalitaPopoverPage),
  ],
  exports: [
    BalitaPopoverPage
  ]
})
export class BalitaPopoverPageModule {}
