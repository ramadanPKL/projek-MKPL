import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TbForm2Page } from './tb-form2';

@NgModule({
  declarations: [
    TbForm2Page,
  ],
  imports: [
    IonicPageModule.forChild(TbForm2Page),
  ],
  exports: [
    TbForm2Page
  ]
})
export class TbForm2PageModule {}
