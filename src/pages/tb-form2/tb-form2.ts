import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
// import {TbForm3Page} from '../tb-form3/tb-form3';

/**
 * Generated class for the BumilFormPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-tb-form2',
  templateUrl: 'tb-form2.html',
})
export class TbForm2Page {
  value: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.value = navParams.get('value');
  }

  nextForm() {
    this.navCtrl.push("TbForm3Page", {
      value: this.value
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TbForm2Page');
  }



}
