import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BumilForm2Page } from './bumil-form2';

@NgModule({
  declarations: [
    BumilForm2Page,
  ],
  imports: [
    IonicPageModule.forChild(BumilForm2Page),
  ],
  exports: [
    BumilForm2Page
  ]
})
export class BumilForm2PageModule {}
