import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Nav } from 'ionic-angular';
import { NccServiceProvider } from '../../providers/ncc-service/ncc-service';

/**
 * Generated class for the BalitaFormPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-balita-form',
  templateUrl: 'balita-form.html',
  // providers: [NccServiceProvider]
})
export class BalitaFormPage {

  public balita: any;
  /* {
    nama_balita: '',
    nama_orang_tua: '',
    idjeniskelamin: '',
    idpuskesmas: '',
  }; */

  data = new FormData();
  currentUser = JSON.parse(localStorage.getItem('currentUser'));

  constructor(public navCtrl: NavController, public navParams: NavParams, private nccService: NccServiceProvider, private alertCtrl: AlertController, private nav: Nav) {
    this.balita = Object.assign({}, navParams.get('balita'));
    this.balita.idpuskesmas = this.currentUser.data.idpuskesmas;
  }

  inputBalita() {
    for (let key in this.balita) {
        this.data.append(key, this.balita[key]);
    }
    if (this.data.has('idbalita')) {
      this.nccService.editBalita(this.data).subscribe((res) => {
        let alert = this.alertCtrl.create({
          title: res.success ? 'Success' : 'Failed',
          subTitle: res.message,
          buttons: ['OK']
        });

        alert.present(prompt);
      });
    } else {
      this.nccService.inputBalita(this.data).subscribe((res) => {
        let alert = this.alertCtrl.create({
          title: res.success? 'Success':'Failed',
          subTitle: res.message,
          buttons: ['OK']
        });
  
        alert.present(prompt);
      });
    }

    this.nav.setRoot("GiziburukListPage");

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BalitaFormPage');
  }

}
