import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BalitaFormPage } from './balita-form';

@NgModule({
  declarations: [
    BalitaFormPage,
  ],
  imports: [
    IonicPageModule.forChild(BalitaFormPage),
  ],
  exports: [
    BalitaFormPage
  ]
})
export class BalitaFormPageModule {}
