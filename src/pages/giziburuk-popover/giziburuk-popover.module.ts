import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GiziburukPopoverPage } from './giziburuk-popover';

@NgModule({
  declarations: [
    GiziburukPopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(GiziburukPopoverPage),
  ],
  exports: [
    GiziburukPopoverPage
  ]
})
export class GiziburukPopoverPageModule {}
