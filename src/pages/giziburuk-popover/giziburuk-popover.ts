import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, ViewController } from 'ionic-angular';

/**
 * Generated class for the GiziburukPopoverPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-giziburuk-popover',
  templateUrl: 'giziburuk-popover.html',
})
export class GiziburukPopoverPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,  private app: App, public viewCtrl: ViewController) {
  }

  addGiziburuk() {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.app.getRootNav().push("GiziburukFormPage");
    this.viewCtrl.dismiss();
  }

  searchGiziburuk() {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.app.getRootNav().push("GiziburukSearchPage");
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GiziburukPopoverPage');
  }

}
