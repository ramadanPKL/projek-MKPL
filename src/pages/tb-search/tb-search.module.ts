import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TbSearchPage } from './tb-search';

@NgModule({
  declarations: [
    TbSearchPage,
  ],
  imports: [
    IonicPageModule.forChild(TbSearchPage),
  ],
  exports: [
    TbSearchPage
  ]
})
export class TbSearchPageModule {}
