import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BumilListPage } from './bumil-list';

@NgModule({
  declarations: [
    BumilListPage,
  ],
  imports: [
    IonicPageModule.forChild(BumilListPage),
  ],
  exports: [
    BumilListPage
  ]
})
export class BumilListPageModule {}
