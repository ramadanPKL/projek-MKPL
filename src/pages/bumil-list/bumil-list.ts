import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { NccServiceProvider } from '../../providers/ncc-service/ncc-service';
import { PopoverController } from 'ionic-angular';
// import { BumilFormPage } from '../bumil-form/bumil-form';
// import { BumilDetailPage } from '../bumil-detail/bumil-detail';
// import { BumilSearchPage } from '../bumil-search/bumil-search';

@IonicPage()
@Component({
  selector: 'page-bumil-list',
  templateUrl: 'bumil-list.html',
  providers: [NccServiceProvider]
})
export class BumilListPage {

  bumilList = [];

  amount: number = 15;
  page: number = 1;
  currentDate = new Date();
  dateString = this.currentDate.getFullYear() + '-' + ('0' + (this.currentDate.getMonth() + 1)).slice(-2) + '-' + ('0' + this.currentDate.getDate()).slice(-2);

  value = {
    tanggal: this.dateString,
  }


  constructor(public navCtrl: NavController, private nccService: NccServiceProvider, public popoverCtrl: PopoverController) {
    this.getBumil();
  }

  getBumil() {
    this.nccService.getBumil(this.amount, this.page).subscribe(data => {
      for (let bumil of data.data) {
        this.bumilList.push(bumil);
      };
    });
  }

  // addBumil() {
  //   this.navCtrl.push("BumilFormPage", {
  //     value: this.value
  //   });
  // }
  //
  // searchBumil() {
  //   this.navCtrl.push("BumilSearchPage");
  // }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');
    // this.amount+=15;
    this.page += 1;

    setTimeout(() => {
      this.getBumil();

      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 1500);

  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.page = 1;

    setTimeout(() => {
      this.bumilList = [];
      this.getBumil();
      console.log('Async operation has ended');
      refresher.complete();
    }, 1500);
  }

  getDetail(item) {
    this.navCtrl.push("BumilDetailPage", {
      item: item
    });
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create("BumilPopoverPage");
    popover.present({
      ev: myEvent
    });
  }

  ionViewDidLeave() {
    let popover = this.popoverCtrl.create("BumilPopoverPage");
    popover.dismiss();
  }

}
