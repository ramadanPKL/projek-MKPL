import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Nav, AlertController } from 'ionic-angular';
import { NccServiceProvider } from '../../providers/ncc-service/ncc-service';
// import { GiziburukFormPage } from '../giziburuk-form/giziburuk-form';
/**
 * Generated class for the GiziburukDetailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-giziburuk-detail',
  templateUrl: 'giziburuk-detail.html',
})
export class GiziburukDetailPage {

  detail: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private nccService: NccServiceProvider, private nav: Nav, private alertCtrl: AlertController) {
    this.detail = navParams.get('item');
  }

  editGiziburuk() {
    this.navCtrl.push("GiziburukFormPage", {
      value: this.detail
    });
  }

  deleteGiziburuk() {
    let confirm = this.alertCtrl.create({
      title: 'Konfirmasi',
      message: `Apakah anda yakin menghapus <b>${this.detail.nama_balita}</b> dari data gizi buruk?`,
      buttons: [
        {
          text: 'Batal',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Ok',
          handler: () => {
            console.log('Agree clicked');
            this.nccService.deleteGiziburuk(this.detail.iddata_gizi_buruk).subscribe((res) => {
              let alert = this.alertCtrl.create({
                title: res.success? 'Success':'Failed',
                subTitle: res.message,
                buttons: ['OK']
              });

              alert.present(prompt);
            });
            this.nav.setRoot("GiziburukListPage");
          }
        }
      ]
    });
    confirm.present();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GiziburukDetailPage');
  }

}
