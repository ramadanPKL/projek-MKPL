import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GiziburukDetailPage } from './giziburuk-detail';

@NgModule({
  declarations: [
    GiziburukDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(GiziburukDetailPage),
  ],
  exports: [
    GiziburukDetailPage
  ]
})
export class GiziburukDetailPageModule {}
