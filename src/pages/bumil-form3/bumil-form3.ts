import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
// import { BumilForm4Page } from '../bumil-form4/bumil-form4';

/**
 * Generated class for the BumilForm3Page page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-bumil-form3',
  templateUrl: 'bumil-form3.html',
})
export class BumilForm3Page {

  value: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.value = navParams.get('value');
  }

  nextForm() {
    this.navCtrl.push("BumilForm4Page", {
      value: this.value
    });
  }

  disableCheck() {
    if (this.value.nama_ibu != '' && this.value.nama_suami != '' && this.value.alamat != '' && this.value.rt != '' && this.value.rw != '' && this.value.no_nik_ibu != '' && this.value.umur_ibu != '') {
      return false;
    } else {
      return true;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BumilForm3Page');
  }

}
