import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BumilForm3Page } from './bumil-form3';

@NgModule({
  declarations: [
    BumilForm3Page,
  ],
  imports: [
    IonicPageModule.forChild(BumilForm3Page),
  ],
  exports: [
    BumilForm3Page
  ]
})
export class BumilForm3PageModule {}
