import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Nav, AlertController} from 'ionic-angular';
import { NccServiceProvider } from '../../providers/ncc-service/ncc-service';
/**
 * Generated class for the TbDetailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-tb-detail',
  templateUrl: 'tb-detail.html',
})
export class TbDetailPage {
  detail : any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private nccService: NccServiceProvider, private nav: Nav, private alertCtrl: AlertController) {
    this.detail = navParams.get('item');
  }


  editTb() {
    this.navCtrl.push("TbFormPage", {
      value: this.detail
    });
  }

  deleteTb() {
    let confirm = this.alertCtrl.create({
      title: 'Konfirmasi',
      message: `Apakah anda yakin menghapus <b>${this.detail.nama_pasien}</b> dari data Tuberculosis?`,
      buttons: [
        {
          text: 'Batal',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Ok',
          handler: () => {
            console.log('Agree clicked');
            this.nccService.deleteTb(this.detail.iddata_tb).subscribe((res) => {
              let alert = this.alertCtrl.create({
                title: res.success? 'Success':'Failed',
                subTitle: res.message,
                buttons: ['OK']
              });

              alert.present(prompt);
            });
            this.nav.setRoot("TbListPage");
          }
        }
      ]
    });
    confirm.present();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TbDetailPage');
  }

}
