import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TbDetailPage } from './tb-detail';

@NgModule({
  declarations: [
    TbDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(TbDetailPage),
  ],
  exports: [
    TbDetailPage
  ]
})
export class TbDetailPageModule {}
