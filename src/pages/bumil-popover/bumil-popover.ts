import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, ViewController } from 'ionic-angular';

/**
 * Generated class for the BumilPopoverPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-bumil-popover',
  templateUrl: 'bumil-popover.html',
})
export class BumilPopoverPage {

  currentDate = new Date();
  dateString = this.currentDate.getFullYear() + '-' + ('0' + (this.currentDate.getMonth() + 1)).slice(-2) + '-' + ('0' + this.currentDate.getDate()).slice(-2);

  value = {
    tanggal: this.dateString,
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, private app: App, public viewCtrl: ViewController) {
  }

  addBumil() {
    this.app.getRootNav().push("BumilFormPage", {
      value: this.value
    });
    this.viewCtrl.dismiss();
  }

  searchBumil() {
    this.app.getRootNav().push("BumilSearchPage");
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BumilPopoverPage');
  }

}
