import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BumilPopoverPage } from './bumil-popover';

@NgModule({
  declarations: [
    BumilPopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(BumilPopoverPage),
  ],
  exports: [
    BumilPopoverPage
  ]
})
export class BumilPopoverPageModule {}
