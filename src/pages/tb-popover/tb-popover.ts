import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Nav, App, ViewController } from 'ionic-angular';
import { NccServiceProvider } from '../../providers/ncc-service/ncc-service';
/**
 * Generated class for the TbPopoverPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-tb-popover',
  templateUrl: 'tb-popover.html',
})
export class TbPopoverPage {
  value: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private app: App, public viewCtrl: ViewController) {
  }

  addTb() {
    this.app.getRootNav().push("TbFormPage", {
      value: this.value
    });
    this.viewCtrl.dismiss();
  }

  searchTb() {
    this.app.getRootNav().push("TbSearchPage");
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TbPopoverPage');
  }

}
