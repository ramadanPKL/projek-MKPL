import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TbPopoverPage } from './tb-popover';

@NgModule({
  declarations: [
    TbPopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(TbPopoverPage),
  ],
  exports: [
    TbPopoverPage
  ]
})
export class TbPopoverPageModule {}
