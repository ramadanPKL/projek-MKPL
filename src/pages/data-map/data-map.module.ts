import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DataMapPage } from './data-map';

@NgModule({
  declarations: [
    DataMapPage,
  ],
  imports: [
    IonicPageModule.forChild(DataMapPage),
  ],
  exports: [
    DataMapPage
  ]
})
export class DataMapPageModule {}
