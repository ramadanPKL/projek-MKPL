import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ToastController } from 'ionic-angular';
import { MapServiceProvider } from '../../providers/map-service/map-service';

/**
 * Generated class for the DataMapPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
let self;

@IonicPage()
@Component({
  selector: 'page-data-map',
  templateUrl: 'data-map.html',
})
export class DataMapPage {

  @ViewChild('map') mapElement;
  map: any;
  selectedRegion = [];
  geojson = {
    kecamatan: {},
    kelurahan: {}
  };
  min = 999999999;
  max = -999999999;
  total = 0;
  intensitas = '';
  tingkat = 'KECAMATAN';
  tmpTingkat = 'KECAMATAN';
  data = 'bumil';

  toast: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private mapService: MapServiceProvider, public toastCtrl: ToastController) {
    // this.toast = toastCtrl;
    self = this;
  }

  change() {
    this.resetData(this.tmpTingkat.toLowerCase());
    this.tmpTingkat = this.tingkat;
    if (this.toast) {
      this.dismissToast();
    }
    this.initMap();
  }

  initMap() {

    if (this.tingkat === 'KECAMATAN') {
      this.fetchGeoJson(() => {
        this.loadMapShapes('kecamatan');
        this.fetchData({
          'selectlist[kecamatan][]': 'kecamatan',
          'grouplist': 'kecamatan',
          'joinlist[0][dest_table]': 'kelurahan',
          'joinlist[0][dest_column]': 'idkelurahan',
          'joinlist[1][dest_table]': 'kecamatan',
          'joinlist[1][dest_column]': 'idkecamatan',
          'joinlist[1][src_table]': 'kelurahan',
      //     'joinlist[2][dest_table]': 'dinkes_rencana_persalinan_biaya',
      //     'joinlist[2][dest_column]': 'idrencana_persalinan_biaya',
      //     'joinlist[3][dest_table]': 'dinkes_rencana_persalinan_kendaraan',
      //     'joinlist[3][dest_column]': 'idrencana_persalinan_kendaraan',
      //     'joinlist[4][dest_table]': 'dinkes_keterangan_bpjs',
      //     'joinlist[4][dest_column]': 'idketerangan_bpjs'
        }, 'kecamatan');
      });
    } else {
      this.fetchGeoJson(() => {
        this.loadMapShapes('kelurahan');
        this.fetchData({
          'selectlist[kelurahan][]': 'kelurahan',
          'grouplist': 'kelurahan',
          'joinlist[0][dest_table]': 'kelurahan',
          'joinlist[0][dest_column]': 'idkelurahan',
          'joinlist[1][dest_table]': 'kecamatan',
          'joinlist[1][dest_column]': 'idkecamatan',
          'joinlist[1][src_table]': 'kelurahan',
          // 'joinlist[2][dest_table]': 'dinkes_rencana_persalinan_biaya',
          // 'joinlist[2][dest_column]': 'idrencana_persalinan_biaya',
          // 'joinlist[3][dest_table]': 'dinkes_rencana_persalinan_kendaraan',
          // 'joinlist[3][dest_column]': 'idrencana_persalinan_kendaraan',
          // 'joinlist[4][dest_table]': 'dinkes_keterangan_bpjs',
          // 'joinlist[4][dest_column]': 'idketerangan_bpjs'
        }, 'kelurahan');
      });
    }

    const mapStyle = [];

    let latLng = new google.maps.LatLng(-7.9666, 112.6446);

    let mapOptions = {
      center: latLng,
      zoom: 12,
      styles: mapStyle,
      // mapTypeId: google.maps.MapTypeId.ROADMAP,
      streetViewControl: false,
    };
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    // set up the style rules and events for google.maps.Data
    // this.map.data.loadGeoJson('assets/ncc-kotamalang.geojson');
    // this.map.data.setStyle({
    //   fillColor: 'green',
    //   strokeWeight: 1
    // });

    console.log(this.styleFeature);
    this.map.data.setStyle(this.styleFeature);
    this.map.data.addListener('mouseover', this.mouseInToRegion);
    this.map.data.addListener('mouseout', this.mouseOutOfRegion);
    // this.map.data.addListener('click', this.mouseClickRegion);
    console.log('MINNNN = ' + this.min);
    console.log('MAXXXX = ' + this.max);

  }
  
  loadMapShapes = (tingkat) => {
    this.map.data.forEach(function (feature) {
      // If you want, check here for some constraints.
      // this.map.data.remove(feature);
    });
    this.map.data.addGeoJson(this.geojson[tingkat], { idPropertyName: tingkat });
  }

  fetchGeoJson = (callback) => {
    // this.setState({ loading: true });
    // const proxy = 'https://cors-anywhere.herokuapp.com/'

    this.mapService.getMap().subscribe((resp) => {
      let kecamatan = {
        "type": "FeatureCollection",
        "features": []
      }
      let kelurahan = {
        "type": "FeatureCollection",
        "features": []
      }

      resp.forEach((v) => {
        kecamatan.features.push(
          {
            "type": "Feature",
            "geometry": {
              "type": "Polygon",
              "coordinates": [
                v.coordinates.map((value) => {
                  return [value.lng, value.lat]
                })
              ]
            },
            "properties": { "kecamatan": v.kecamatan }
          }
        )
        v.properties.forEach((v) => {
          kelurahan.features.push(
            {
              "type": "Feature",
              "geometry": {
                "type": "Polygon",
                "coordinates": [
                  v.coordinates.map((value) => {
                    return [value.lng, value.lat]
                  })
                ]
              },
              "properties": { "kelurahan": v.kelurahan }
            }
          )
        })
      })

      console.log(kecamatan);
      console.log(kelurahan);

      this.geojson.kecamatan = kecamatan;
      this.geojson.kelurahan = kelurahan;

      // this.state.geojson = {
      //   kecamatan: kecamatan,
      //   kelurahan: kelurahan
      // };
      // this.setState({
      // })
      callback();
    });

  }

  fetchData = (params = {}, tingkat) => {

    if (this.data === 'bumil') {
      this.mapService.getBumil(params).subscribe((resp) => {
        console.log('responessssss');
        console.log(resp);
        if (resp.success) {
          // infowindow.close()
          // this.setState({ selectedRegion: [] })
  
          this.selectedRegion = [];
          // this.state = { selectedRegion: [] };
  
          let min = this.min
          let max = this.max
          let total = 0
  
          resp.data.forEach((v) => {
            let c_jumlah = parseInt(v.jumlah, 10);
            console.log(c_jumlah);
  
            if (v[tingkat] !== 'LAIN-LAIN') {
              console.log(tingkat, v[tingkat]);
              this.map.data.getFeatureById(v[tingkat]).setProperty('jumlah', c_jumlah);
            }
            total += c_jumlah;
          });
  
          this.geojson[tingkat].features.forEach((v) => {
            if (v.properties[tingkat] !== 'LAIN-LAIN') {
              let c_jumlah = this.map.data.getFeatureById(v.properties[tingkat]).getProperty('jumlah');
              if (c_jumlah < min) { min = c_jumlah; }
              if (c_jumlah > max) { max = c_jumlah; }
            }
          });
  
          console.log('MIN = ' + min);
          console.log('MAX = ' + max);
  
          this.total = total;
          this.min = min;
          this.max = max;
  
          console.log('MIN new = ' + this.min);
          console.log('MAX new = ' + this.max);
  
          // this.state = { total: total, min: min, max: max };
  
          // this.setState({ total: total, min: min, max: max })
        }
  
      });
    } else if (this.data === 'tb') {
      this.mapService.getTb(params).subscribe((resp) => {
        console.log('responessssss');
        console.log(resp);
        if (resp.success) {
          // infowindow.close()
          // this.setState({ selectedRegion: [] })

          this.selectedRegion = [];
          // this.state = { selectedRegion: [] };

          let min = this.min
          let max = this.max
          let total = 0

          resp.data.forEach((v) => {
            let c_jumlah = parseInt(v.jumlah, 10);
            console.log(c_jumlah);

            if (v[tingkat] !== 'LAIN-LAIN') {
              console.log(tingkat, v[tingkat]);
              this.map.data.getFeatureById(v[tingkat]).setProperty('jumlah', c_jumlah);
            }
            total += c_jumlah;
          });

          this.geojson[tingkat].features.forEach((v) => {
            if (v.properties[tingkat] !== 'LAIN-LAIN') {
              let c_jumlah = this.map.data.getFeatureById(v.properties[tingkat]).getProperty('jumlah');
              if (c_jumlah < min) { min = c_jumlah; }
              if (c_jumlah > max) { max = c_jumlah; }
            }
          });

          console.log('MIN = ' + min);
          console.log('MAX = ' + max);

          this.total = total;
          this.min = min;
          this.max = max;

          console.log('MIN new = ' + this.min);
          console.log('MAX new = ' + this.max);

          // this.state = { total: total, min: min, max: max };

          // this.setState({ total: total, min: min, max: max })
        }

      });
    } else {
      this.mapService.getGiziburuk(params).subscribe((resp) => {
        console.log('responessssss');
        console.log(resp);
        if (resp.success) {
          // infowindow.close()
          // this.setState({ selectedRegion: [] })

          this.selectedRegion = [];
          // this.state = { selectedRegion: [] };

          let min = this.min
          let max = this.max
          let total = 0

          resp.data.forEach((v) => {
            let c_jumlah = parseInt(v.jumlah, 10);
            console.log(c_jumlah);

            if (v[tingkat] !== 'LAIN-LAIN') {
              console.log(tingkat, v[tingkat]);
              this.map.data.getFeatureById(v[tingkat]).setProperty('jumlah', c_jumlah);
            }
            total += c_jumlah;
          });

          this.geojson[tingkat].features.forEach((v) => {
            if (v.properties[tingkat] !== 'LAIN-LAIN') {
              let c_jumlah = this.map.data.getFeatureById(v.properties[tingkat]).getProperty('jumlah');
              if (c_jumlah < min) { min = c_jumlah; }
              if (c_jumlah > max) { max = c_jumlah; }
            }
          });

          console.log('MIN = ' + min);
          console.log('MAX = ' + max);

          this.total = total;
          this.min = min;
          this.max = max;

          console.log('MIN new = ' + this.min);
          console.log('MAX new = ' + this.max);

          // this.state = { total: total, min: min, max: max };

          // this.setState({ total: total, min: min, max: max })
        }

      });
    }

  }

  resetData = (tingkat) => {

    this.geojson[tingkat].features.forEach((v) => {
      if (v.properties[tingkat] !== 'LAIN-LAIN') {
        this.map.data.getFeatureById(v.properties[tingkat]).setProperty('jumlah', 0)
      }
    })

    this.min = 999999999;
    this.max = -999999999;
    this.total = 0;
    // this.setState({
    // })
  }

  styleFeature(feature) {
    
    const min = self.min;
    const max = self.max;

    // delta represents where the value sits between the min and max
    const delta = (feature.getProperty('jumlah') - min) / (max - min);
    console.log(max, min, delta, feature.getProperty('kelurahan'), feature.getProperty('jumlah'))
    let color = '';
    //let intensitas = ''
    if (isNaN(feature.getProperty('jumlah'))) {
      color = '#87d37c'
      feature.setProperty('intensitas', 'kosong')
    } //kosong
    else if (delta <= 0.2) {
      color = '#16a085';
      feature.setProperty('intensitas', 'rendah_sekali')
    } //rendah sekali
    else if (delta <= 0.4) {
      color = '#f1c40f';
      feature.setProperty('intensitas', 'rendah')
    } //rendah
    else if (delta <= 0.6) {
      color = '#f39c12';
      feature.setProperty('intensitas', 'menengah')
    } //menengah
    else if (delta <= 0.8) {
      color = '#d35400';
      feature.setProperty('intensitas', 'tinggi')
    } //tingi
    else {
      color = '#c0392b';
      feature.setProperty('intensitas', 'tinggi_sekali')
    } //tinggi sekali 

    // determine whether to show this shape or not
    let showRow = true;
    // if (feature.getProperty('jumlah') == null || isNaN(feature.getProperty('jumlah'))) {
    //   showRow = false;
    // }

    let outlineWeight = 0.5, zIndex = 1;
    if (feature.getProperty('state') === 'hover') {
      outlineWeight = zIndex = 2;
    }

    return {
      strokeWeight: outlineWeight,
      strokeColor: '#fff',
      zIndex: zIndex,
      fillColor: color,
      fillOpacity: 0.8,
      visible: showRow
    }
  }
  
  presentToast(lokasi, jumlah, intensitas) {
    this.toast = this.toastCtrl.create({
      message: this.tingkat + ' : ' + lokasi + ' | JUMLAH : ' + jumlah + ' | INTENSITAS : ' + intensitas,
      showCloseButton: true,
      closeButtonText: 'OK',
      dismissOnPageChange: true
    });
    this.toast.present();
  }

  dismissToast() {
    this.toast.dismiss();
  }

  /* mouseClickRegion(e) {
    const tingkat = self.tingkat.toLowerCase();

    const label = e.feature.getProperty(tingkat);
    const jumlah = e.feature.getProperty('jumlah');
    const intensitas = e.feature.getProperty('intensitas');

    self.selectedRegion = [{
      label: label,
      value: jumlah,
      color: "#007bb6"
    },
    {
      label: 'MALANG',
      value: self.total - jumlah,
      color: '#00aced'
    }
    ];
    self.intensitas = intensitas;

    self.presentToast(label, jumlah);

  } */

  mouseInToRegion(e) {
    const tingkat = self.tingkat.toLowerCase();

    const label = e.feature.getProperty(tingkat);
    const jumlah = e.feature.getProperty('jumlah');
    const intensitas = e.feature.getProperty('intensitas');

    self.selectedRegion = [{
      label: label,
      value: jumlah,
      color: "#007bb6"
    },
    {
      label: 'MALANG',
      value: self.total - jumlah,
      color: '#00aced'
    }
    ];
    self.intensitas = intensitas;

    self.presentToast(label, jumlah?jumlah:0, intensitas);
    e.feature.setProperty('state', 'hover');
  }
  
  mouseOutOfRegion(e) {
    self.dismissToast();
    e.feature.setProperty('state', 'normal');
  }


  ionViewDidEnter() {
    this.initMap();
  }

}
