import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GiziburukListPage } from './giziburuk-list';

@NgModule({
  declarations: [
    GiziburukListPage,
  ],
  imports: [
    IonicPageModule.forChild(GiziburukListPage),
  ],
  exports: [
    GiziburukListPage
  ]
})
export class GiziburukListPageModule {}
