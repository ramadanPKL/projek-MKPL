import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';

import { NccServiceProvider } from '../../providers/ncc-service/ncc-service';
// import { BalitaFormPage } from '../balita-form/balita-form';
// import { GiziburukFormPage } from '../giziburuk-form/giziburuk-form';
// import { GiziburukDetailPage } from '../giziburuk-detail/giziburuk-detail';
// import { GiziburukSearchPage } from '../giziburuk-search/giziburuk-search';

/**
 * Generated class for the GiziburukListPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-giziburuk-list',
  templateUrl: 'giziburuk-list.html',
})
export class GiziburukListPage {

  giziburukList = [];
  balitaList = [];
  amount = 15;
  page = 1;
  list = 'giziburuk';

  constructor(public navCtrl: NavController, public navParams: NavParams, private nccService: NccServiceProvider, private popoverCtrl: PopoverController) {
    this.getGiziburuk();
    this.getBalita();
  }

  // addBalita() {
  //   // Reset the content nav to have just this page
  //   // we wouldn't want the back button to show in this scenario
  //   this.navCtrl.push("BalitaFormPage");
  // }
  //
  // addGiziburuk() {
  //   // Reset the content nav to have just this page
  //   // we wouldn't want the back button to show in this scenario
  //   this.navCtrl.push("GiziburukFormPage");
  // }
  //
  // searchGiziburuk() {
  //   // Reset the content nav to have just this page
  //   // we wouldn't want the back button to show in this scenario
  //   this.navCtrl.push("GiziburukSearchPage");
  // }

  getGiziburuk() {
    this.nccService.getGiziburuk(this.amount, this.page).subscribe(data => { for(let gizbur of data.data) {
          this.giziburukList.push(gizbur);
        };});
  }

  getBalita() {
    this.nccService.getBalita().subscribe(data => { for(let balita of data.data.sort().reverse()) {
          this.balitaList.push(balita);
        };});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GiziburukListPage');
  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');
    // this.amount+=15;
    this.page+=1;

    setTimeout(() => {
      this.getGiziburuk();

      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 1500);
  }

  giziburukPopover(myEvent) {
    let popover = this.popoverCtrl.create("GiziburukPopoverPage");
    popover.present({
      ev: myEvent
    });
  }

  balitaPopover(myEvent) {
    let popover = this.popoverCtrl.create("BalitaPopoverPage");
    popover.present({
      ev: myEvent
    });
  }

  balitaOptions(myEvent, balita) {
    let popover = this.popoverCtrl.create("BalitaOptionsPage", {
      'balita': balita,
    });
    popover.present({
      ev: myEvent
    });
  }

  getDetail(item) {
    this.navCtrl.push("GiziburukDetailPage", {
      item:item
    });
  }

}
