import { Component } from '@angular/core';

import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading, MenuController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  //providers: [NccServiceProvider]
})
export class LoginPage {
  loading: Loading;

  body = { username: '', password: '' };
  //private headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});

  constructor(private nav: NavController, private auth: AuthService, private alertCtrl: AlertController, private loadingCtrl: LoadingController, private menu: MenuController) {
    this.menu.swipeEnable(false);
  }

  // public createAccount() {
  //   this.nav.push('RegisterPage');
  // }

  public login() {

    this.showLoading()
    this.auth.login(this.body).subscribe(data => {
      if (data.success) {
        localStorage.setItem('currentUser', JSON.stringify(data));
        this.menu.swipeEnable(true);
        this.nav.setRoot("BumilListPage");
      } else {
        this.showError("Access Denied");
      }
    },
      error => {
        this.showError(error);
      });
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  showError(text) {
    this.loading.dismiss();

    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }
  // constructor(public navCtrl: NavController, public navParams: NavParams) {
  // }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  ionViewDidEnter(){
    localStorage.removeItem('currentUser');
  }
}
