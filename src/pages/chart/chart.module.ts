import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChartPage } from './chart';
// import { Chart } from 'chart.js';
// import {ChartsModule} from 'ng2-charts/charts/charts';

@NgModule({
  declarations: [
    ChartPage,
  ],
  imports: [
    IonicPageModule.forChild(ChartPage),
    // ChartsModule
  ],
  exports: [
    ChartPage
  ]
})
export class ChartPageModule {}
