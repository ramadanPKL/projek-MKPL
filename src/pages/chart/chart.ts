import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Nav, Platform} from 'ionic-angular';
import { RequestOptions, URLSearchParams } from '@angular/http';
import { Chart } from 'chart.js';
import { ChartServiceProvider } from '../../providers/chart-service/chart-service';

/**
 * Generated class for the ChartPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-chart',
  templateUrl: 'chart.html',
  providers: [ChartServiceProvider]
})

export class ChartPage {
    
    chartOptions: any = {
        scaleShowVerticalLines: false,
        responsive: true
    };
    
    chartLabels = [];
    chartData = [];
    chartKategori = [];

    startdate: any;
    enddate: any;

    selectlist: any;
    grouplist: any;
    keySelectList: any;
    keyGroupList: any;

    subskpd: any;

    barChart: any;
    dataset: any;
    
    value: any = {
    };

    directive: any = {
    };
    
    data: any= {
    };

  @ViewChild('barCanvas') barCanvas;


  constructor(public navCtrl: NavController, public navParams: NavParams, private chartService: ChartServiceProvider, private alertCtrl: AlertController, public platform: Platform) {
    
  }

  getDataGrafik(){
    this.defineChartData();
    
  }


  defineChartData()
  {
    let params: URLSearchParams = new URLSearchParams();
    //Subskpd(Data bumil, gizbur, atau tb)
    this.subskpd = this.value.idsubskpd;
    // this.subskpd = 1;
    // this.selectlist = 'year(tanggal) as tahun';
    // this.grouplist = 'year(tanggal)';

    if(this.value.idberdasarkan == 1){
        this.selectlist = 'year(tanggal) as tahun';
        this.grouplist = 'year(tanggal)';
        
        if(this.value.tahun && !this.directive.checkTahun){
            var startdate = new Date(this.value.tahun, 0, 1);
            var enddate = new Date(this.value.tahun, 11, 31);
            this.startdate = startdate.toISOString().substring(0, 10);
            this.enddate = enddate.toISOString().substring(0, 10);
            // this.selectlist = 'year(tanggal) as tahun';
            // this.grouplist = 'year(tanggal)';

            console.log("startdate: "+this.startdate);
            console.log("enddate: "+this.enddate);

            params.set('wherelist[0][tanggal][startdate]', this.startdate);
            params.set('wherelist[0][tanggal][enddate]', this.enddate);
            params.set('selectlist[skip]', this.selectlist);
            // params.set('grouplist[]', this.grouplist);
        }else{
            params.set('selectlist[skip]', this.selectlist);
            // params.set('grouplist[]', this.grouplist);
        }
        
    }
    else if(this.value.idberdasarkan == 2){
        this.selectlist = 'month(tanggal) as bulan';
        this.grouplist = 'month(tanggal)';
        if(this.value.bulan && this.value.tahun){
            var startdate = new Date(this.value.tahun, this.value.bulan, 1);
            var enddate = new Date(this.value.tahun, this.value.bulan + 1, 0);
            this.startdate = startdate.toISOString().substring(0, 10);
            this.enddate = enddate.toISOString().substring(0, 10);
            // this.selectlist = 'month(tanggal) as bulan';
            // this.grouplist = 'month(tanggal)';

            console.log("startdate: "+this.startdate);
            console.log("enddate: "+this.enddate);

            params.set('selectlist[skip]', this.selectlist);
            // params.set('grouplist[]', this.grouplist);

            params.set('wherelist[0][tanggal][startdate]', this.startdate);
            params.set('wherelist[0][tanggal][enddate]', this.enddate);
        }else{
            params.set('selectlist[skip]', this.selectlist);
            // params.set('grouplist[]', this.grouplist);
        }
    }

    // Set parameter GET
    if(this.value.parameter != 1){
        // this.keyGroupList = '';
        var joinListTable: any;
        var joinListColumn: any;

        if (this.value.parameter == "Kelurahan") {
            joinListTable = "kelurahan";
            joinListColumn = "idkelurahan";    
        } 
        else if(this.value.parameter == "Kecamatan"){
            joinListTable = "kecamatan";
            joinListColumn = "idkecamatan";    
        }
        else if(this.value.parameter == "Puskesmas"){
            joinListTable = "puskesmas";
            joinListColumn = "idpuskesmas";    
        }

        var keySelectList = "selectlist["+joinListTable+"]";

        params.set('joinlist[0][dest_table]', joinListTable);    
        params.set('joinlist[0][dest_column]', joinListColumn);  
        params.set(keySelectList, joinListTable+" as kategori")  
        
        params.set("grouplist[]'[0]", this.grouplist);
        params.set("grouplist[]'[1]", joinListTable);
    }
    else{
        // params.set('grouplist[]', this.grouplist);
    }
    // params.set('grouplist[]', this.grouplist);
    if(this.value.parameter == 1){
        params.set('grouplist[]', this.grouplist);

    }
    params.set('selectlist[skip]', this.selectlist);
    
    params.set('no_lain', '0');

    let requestOptions = new RequestOptions();
    requestOptions.params = params;

    
    this.chartService.chartBumil(requestOptions, this.subskpd).subscribe(datagraf => {
        this.data = datagraf.data;

        for(let i = 0, l = this.data.length; i < l; i++) {

            var datajson = this.data[i];
            var label = "";
            // var label = datajson.tahun;
            if(this.value.idberdasarkan == 1){
                label = datajson.tahun;
                
            }else{
                label = datajson.bulan;
            }
            var jumlah = datajson.jumlah;
            var kategori = "";
            if(datajson.kategori){
                kategori = datajson.kategori;
                // this.chartKategori = kategori;
            }
            console.log("tahun " + label);
            console.log("jumlah " + datajson.jumlah);
            this.chartLabels.push(label);
            this.chartData.push(jumlah);
            this.chartKategori.push(kategori);
        }
    });

    this.loadChart();
  }

  loadChart(){

    // console.log("tes"+this.chartData);
    // console.log("tes"+this.chartLabels);

    // if (!this.barCanvas) { return; }
    this.barChart = new Chart(this.barCanvas.nativeElement , {
        type: 'horizontalBar',
        data: {
            labels: this.chartLabels,
            datasets: [{
                label: this.chartKategori,
                data: this.chartData,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 2
            }]
        },
        options: {
            scales: {
                xAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }],
                yAxes: [{barPercentage :0.5}]
                
            }
        }

    });
  }

  chartUpdate(){
    console.log('update chart');
    this.getDataGrafik();
    this.barChart.update;

    this.chartLabels = [];
    this.chartData = [];
    this.chartKategori = [];    
  }  
  
//   ionViewDidEnter() {
//     console.log('ionViewDidLoad ChartPage');
//     this.getDataGrafik();
//     this.barChart.update;
//   }

}
