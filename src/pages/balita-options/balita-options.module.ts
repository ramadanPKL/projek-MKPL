import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BalitaOptionsPage } from './balita-options';

@NgModule({
  declarations: [
    BalitaOptionsPage,
  ],
  imports: [
    IonicPageModule.forChild(BalitaOptionsPage),
  ],
  exports: [
    BalitaOptionsPage
  ]
})
export class BalitaOptionsPageModule {}
