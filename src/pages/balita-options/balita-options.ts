import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, App, AlertController } from 'ionic-angular';
import { NccServiceProvider } from '../../providers/ncc-service/ncc-service';

/**
 * Generated class for the BalitaOptionsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-balita-options',
  templateUrl: 'balita-options.html',
})
export class BalitaOptionsPage {

  selectedBalita: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private app: App, private viewCtrl: ViewController, private nccService: NccServiceProvider, private alertCtrl: AlertController) {
    this.selectedBalita =  navParams.get('balita');
    console.log(this.selectedBalita.idbalita);
  }

  editBalita() {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.app.getRootNav().push('BalitaFormPage', {
      'balita': this.selectedBalita,
    });
    this.viewCtrl.dismiss();
  }

  deleteBalita() {
    let confirm = this.alertCtrl.create({
      title: 'Konfirmasi',
      message: `Apakah anda yakin menghapus <b>${this.selectedBalita.nama_balita}</b> dari data balita?`,
      buttons: [
        {
          text: 'Batal',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Ok',
          handler: () => {
            console.log('Agree clicked');
            this.nccService.deleteBalita(this.selectedBalita.idbalita).subscribe((res) => {
              let alert = this.alertCtrl.create({
                title: res.success ? 'Success' : 'Failed',
                subTitle: res.message,
                buttons: ['OK']
              });

              alert.present(prompt);
            });
            this.app.getRootNav().setRoot("GiziburukListPage");
            // this.navCtrl.setRoot("GiziburukListPage");
            // this.navCtrl.setRoot(this.navCtrl.getActive().component);
            // this.nav.goToRoot();
            // this.nav.setRoot("GiziburukListPage");
          }
        }
      ]
    });
    confirm.present();
    this.viewCtrl.dismiss();    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BalitaOptionsPage');
  }

}
