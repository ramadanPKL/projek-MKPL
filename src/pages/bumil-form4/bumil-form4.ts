import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Nav, AlertController } from 'ionic-angular';
import { NccServiceProvider } from '../../providers/ncc-service/ncc-service';

/**
 * Generated class for the BumilForm4Page page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-bumil-form4',
  templateUrl: 'bumil-form4.html',
})
export class BumilForm4Page {

  value: any;
  data = new FormData();

  constructor(public navCtrl: NavController, public navParams: NavParams, private nccService: NccServiceProvider, private nav: Nav, private alertCtrl: AlertController) {
    this.value = navParams.get('value');
  }

  confirmBumil() {
    for (let key in this.value) {
      this.data.append(key, this.value[key]);
    }

    if (this.value.iddata_bumil) {
      this.nccService.editBumil(this.data).subscribe((res) => {
        let alert = this.alertCtrl.create({
          title: res.success? 'Success':'Failed',
          subTitle: res.message,
          buttons: ['OK']
        });

        alert.present(prompt);
      });
    } else {
      this.nccService.inputBumil(this.data).subscribe((res) => {
        let alert = this.alertCtrl.create({
          title: res.success? 'Success':'Failed',
          subTitle: res.message,
          buttons: ['OK']
        });

        alert.present(prompt);
      });
    }
    this.nav.setRoot("BumilListPage");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BumilForm4Page');
  }

}
