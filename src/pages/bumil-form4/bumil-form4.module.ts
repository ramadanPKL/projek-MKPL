import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BumilForm4Page } from './bumil-form4';

@NgModule({
  declarations: [
    BumilForm4Page,
  ],
  imports: [
    IonicPageModule.forChild(BumilForm4Page),
  ],
  exports: [
    BumilForm4Page
  ]
})
export class BumilForm4PageModule {}
