import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NccServiceProvider } from '../../providers/ncc-service/ncc-service';
// import { BumilDetailPage } from '../bumil-detail/bumil-detail';

/**
 * Generated class for the BumilSearchPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-bumil-search',
  templateUrl: 'bumil-search.html',
})
export class BumilSearchPage {

  bumilList = [];
  body = new FormData();
  length = 15;
  start = 15;

  constructor(public navCtrl: NavController, public navParams: NavParams, private nccService: NccServiceProvider) {
    this.body.append('draw', '1');
    this.body.append('length', this.length.toString());
    this.body.append('order[0][column]', '4');
    this.body.append('order[0][dir]', 'asc');
  }

  getItems(ev: any) {

    // set val to the value of the searchbar
    let val = ev.target.value;

    this.body.append('start', '0');
    this.body.append('search[value]', val);

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.nccService.searchBumil(this.body).subscribe(data => {
        this.bumilList = data.data;
      });
    } else {
      this.bumilList.length = 0;
    }
  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');
    this.start +=5;
    this.body.append('start', this.start.toString());

    setTimeout(() => {
      this.nccService.searchBumil(this.body).subscribe(data => {
        for (let bumil of data.data) {
          this.bumilList.push(bumil);
        };
      });

      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 1500);

  }

  getDetail(item) {
    this.navCtrl.push("BumilDetailPage", {
      item: item
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BumilSearchPage');
  }

}
