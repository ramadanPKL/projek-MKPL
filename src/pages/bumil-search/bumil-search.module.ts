import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BumilSearchPage } from './bumil-search';

@NgModule({
  declarations: [
    BumilSearchPage,
  ],
  imports: [
    IonicPageModule.forChild(BumilSearchPage),
  ],
  exports: [
    BumilSearchPage
  ]
})
export class BumilSearchPageModule {}
