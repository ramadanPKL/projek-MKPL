import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, ToastController, NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// import { BumilListPage } from '../pages/bumil-list/bumil-list';
// import { TbListPage } from '../pages/tb-list/tb-list'
// import { GiziburukListPage } from '../pages/giziburuk-list/giziburuk-list';
// import { PscFormPage } from '../pages/psc-form/psc-form';

// import { LoginPage } from '../pages/login/login';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: string = "LoginPage";
  activePage: any;

  pages: Array<{title: string, icon: string, component: string}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public toastCtrl: ToastController, navCtrl: NavController) {
    this.initializeApp();

    platform.ready().then(() => {

      var lastTimeBackPress = 0;
      var timePeriodToExit = 2000;

      platform.registerBackButtonAction(() => {

        if (this.nav.canGoBack()) {
          // console.log("can go back");
          this.nav.pop();
        } else {
          if (new Date().getTime() - lastTimeBackPress < timePeriodToExit) {
            this.platform.exitApp(); //Exit from app
          } else {
            let toast = this.toastCtrl.create({
              message: 'Tekan back sekali lagi untuk keluar',
              duration: 3000,
              position: 'bottom'
            });
            toast.present();
            lastTimeBackPress = new Date().getTime();
          }
          // console.log("cant go back");
          // this.platform.exitApp();
        }
      });

      // console.log(this.nav.canGoBack());
    });

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Ibu Hamil', icon: 'woman', component: "BumilListPage" },
      { title: 'Tuberculosis', icon: 'no-smoking', component: "TbListPage" },
      { title: 'Gizi Buruk', icon: 'sad', component: "GiziburukListPage" },
      // { title: 'Public Safety Center', icon: 'bonfire', component: "PscFormPage" },
      { title: 'Grafik', icon: 'analytics', component: "ChartPage" },
      { title: 'Maps', icon: 'map', component: "DataMapPage" },
      { title: 'Logout', icon: 'close', component: "LoginPage" }
      
    ];

    this.activePage = this.pages[0];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if (page != this.activePage) {
      this.nav.setRoot(page.component);
      this.activePage = page;
    }
  }

  checkActive(page) {
    return page == this.activePage;
  }

}
