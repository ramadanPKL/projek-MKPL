import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule }    from '@angular/http';
//import {FormsModule} from '@angular/forms';
import {Http, Response} from '@angular/http';
import { MyApp } from './app.component';

// import { BumilListPage } from '../pages/bumil-list/bumil-list';
// import { BumilFormPage } from '../pages/bumil-form/bumil-form';
// import { BumilForm2Page } from '../pages/bumil-form2/bumil-form2';
// import { BumilForm3Page } from '../pages/bumil-form3/bumil-form3';
// import { BumilForm4Page } from '../pages/bumil-form4/bumil-form4';
// import { BumilDetailPage } from '../pages/bumil-detail/bumil-detail';
// import { BumilSearchPage } from '../pages/bumil-search/bumil-search';
//
// import { TbListPage } from '../pages/tb-list/tb-list';
// import { PscFormPage } from '../pages/psc-form/psc-form';
// //import {dinkesService} from './dinkes.service';
// import { TbFormPage } from '../pages/tb-form/tb-form';
// import { TbForm1Page } from '../pages/tb-form1/tb-form1';
// import { TbForm2Page } from '../pages/tb-form2/tb-form2';
// import { TbForm3Page } from '../pages/tb-form3/tb-form3';
//
// import { GiziburukListPage } from '../pages/giziburuk-list/giziburuk-list';
// import { GiziburukFormPage } from '../pages/giziburuk-form/giziburuk-form';
// import { GiziburukForm2Page } from '../pages/giziburuk-form2/giziburuk-form2';
// import { BalitaFormPage } from '../pages/balita-form/balita-form';
// import { GiziburukDetailPage } from '../pages/giziburuk-detail/giziburuk-detail';
// import { GiziburukSearchPage } from '../pages/giziburuk-search/giziburuk-search';
//
// import { LoginPage } from '../pages/login/login';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { NccServiceProvider } from '../providers/ncc-service/ncc-service';

import { AuthService } from '../providers/auth-service/auth-service';
import { ChartServiceProvider } from '../providers/chart-service/chart-service';
import { MapServiceProvider } from '../providers/map-service/map-service';
@NgModule({
  declarations: [
    MyApp,

    // BumilListPage,
    // BumilFormPage,
    // BumilForm2Page,
    // BumilForm3Page,
    // BumilForm4Page,
    // BumilDetailPage,
    // BumilSearchPage,
    //
    // TbListPage,
    // PscFormPage,
    // TbFormPage,
    // TbForm1Page,
    // TbForm2Page,
    // TbForm3Page,
    //
    // GiziburukListPage,
    // GiziburukFormPage,
    // GiziburukForm2Page,
    // BalitaFormPage,
    // GiziburukDetailPage,
    // GiziburukSearchPage,
    //
    // LoginPage

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    //FormsModule,
    HttpModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    // BumilListPage,
    // BumilFormPage,
    // BumilForm2Page,
    // BumilForm3Page,
    // BumilForm4Page,
    // BumilDetailPage,
    // BumilSearchPage,
    //
    // TbListPage,
    //
    // TbFormPage,
    // TbForm1Page,
    // TbForm2Page,
    // TbForm3Page,
    //
    // GiziburukListPage,
    // GiziburukFormPage,
    // GiziburukForm2Page,
    // BalitaFormPage,
    // GiziburukDetailPage,
    // GiziburukSearchPage,
    //
    // PscFormPage,
    //
    // LoginPage

  ],
  providers: [
    AuthService,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    NccServiceProvider,
    ChartServiceProvider,
    MapServiceProvider
  ]
})
export class AppModule {}
