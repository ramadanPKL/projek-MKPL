import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';

export class User {

  username: string;

  constructor( username: string) {
    this.username = username;
  }
}

@Injectable()
export class AuthService {
  //currentUser: User;
  url= "http://simkesma.sasindo.id/sys/app/authenticate/"
  // url= "/sys/app/authenticate/"

  constructor(private http: Http) {
  }

  private headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});

  login(credential) {
    let body = `username=${credential.username}&password=${credential.password}`;

    // let headers = new Headers({ 'Content-Type': 'application/json' });
    //     let options = new RequestOptions({ headers: headers });
    return this.http.post(this.url , body, { headers: this.headers })
      .do(this.logResponse)
      .map(this.extractData)
      .do(this.logResponse)
      .catch(this.catchError)
  }

  private catchError(error: Response | any) {
    console.log(error);
    return Observable.throw(error.json().error || "Server error.");
  }

  private logResponse(res: Response) {
    console.log(res);
  }

  private extractData(res: Response) {
    return res.json();
  }



  // public login(credentials) {
  //
  //   return this.http.post(this.url, credentials)
  //       .map((response: Response) => {
  //
  //           let user = response.json();
  //           if (user.success ) {
  //               console.log(user.success);
  //               this.currentUser = user;
  //           }
  //             return user;
  //       });
  //
  //   }

  // public login(credentials) {
  //   if (credentials.username === null || credentials.password === null) {
  //     return Observable.throw("Please insert credentials");
  //   } else {
  //     return Observable.create(observer => {
  //       // At this point make a request to your backend to make a real check!
  //       let access = (credentials.password === "pass" && credentials.username === "username");
  //       this.currentUser = new User('Simon', 'saimon@devdactic.com');
  //       observer.next(access);
  //       observer.complete();
  //     });
  //   }
  // }

  // public register(credentials) {
  //   if (credentials.username === null || credentials.password === null) {
  //     return Observable.throw("Please insert credentials");
  //   } else {
  //     // At this point store the credentials to your backend!
  //     return Observable.create(observer => {
  //       observer.next(true);
  //       observer.complete();
  //     });
  //   }
  // }
  //
  // public getUserInfo() : User {
  //   return this.currentUser;
  // }
  //
  // public logout() {
  //   return Observable.create(observer => {
  //     this.currentUser = null;
  //     observer.next(true);
  //     observer.complete();
  //   });
  // }
}
