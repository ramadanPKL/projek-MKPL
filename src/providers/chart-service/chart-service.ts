import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';

/*
  Generated class for the ChartServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
interface myObj{
    string: myObj[];
}

@Injectable()
export class ChartServiceProvider {

  // for mobile
  private url = 'http://simkesma.sasindo.id/map/data';

  // for web
  // private url = '/map/data';

  constructor(public http: Http) {
    console.log('Hello ChartServiceProvider Provider');
  }
  // if(){
  //
  // }
  // else if(){
  //
  // }
  //
  chartBumil(request, subskpd) {
    // let obj: myObj = JSON.parse(data.toString());
    return this.http.get(this.url + '//'+subskpd+'/', request)
      .do(this.logResponse)
      .map(this.extractData)
      .do(this.logResponse)
      .catch(this.catchError)
  }

  /*-----------------------------------
  RETURN ERROR, RESPONSE & DATA
  -----------------------------------*/

  private catchError(error: Response | any) {
    console.log(error);
    return Observable.throw(error.json().error || "Server error.");
  }

  private logResponse(res: Response) {
    console.log(res);
  }

  private extractData(res: Response) {
    return res.json();
  }

}
