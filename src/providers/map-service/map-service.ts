import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the MapServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class MapServiceProvider {

  url = "http://simkesma.sasindo.id/map/data/"
  // url= "/map/data/"

  constructor(public http: Http) {
    console.log('Hello MapServiceProvider Provider');
  }

  getMap() {
    return this.http.get("assets/simkesmas-kotamalang.geojson")
      .map((res: Response) => res.json()); //records in this case
  }

  getBumil(params) {
    let param: URLSearchParams = new URLSearchParams();
    for (let key in params) {
      param.set(key, params[key]);
    }
    // param.set('selectlist[skip]', 'year(tanggal) as tahun');
    // param.set('grouplist[]', 'year(tanggal)');
    // param.set('no_lain', '0');

    let requestOptions = new RequestOptions();
    requestOptions.params = param;

    return this.http.get(this.url + "1", requestOptions)
      .map((res: Response) => res.json()); //records in this case
  }

  getTb(params) {
    let param: URLSearchParams = new URLSearchParams();
    for (let key in params) {
      param.set(key, params[key]);
    }
    // param.set('selectlist[skip]', 'year(tanggal) as tahun');
    // param.set('grouplist[]', 'year(tanggal)');
    // param.set('no_lain', '0');

    let requestOptions = new RequestOptions();
    requestOptions.params = param;

    return this.http.get(this.url + "3", requestOptions)
      .map((res: Response) => res.json()); //records in this case
  }

  getGiziburuk(params) {
    let param: URLSearchParams = new URLSearchParams();
    for (let key in params) {
      param.set(key, params[key]);
    }
    // param.set('selectlist[skip]', 'year(tanggal) as tahun');
    // param.set('grouplist[]', 'year(tanggal)');
    // param.set('no_lain', '0');

    let requestOptions = new RequestOptions();
    requestOptions.params = param;

    return this.http.get(this.url + "2", requestOptions)
      .map((res: Response) => res.json()); //records in this case
  }

}
