import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';

/*
  Generated class for the NccServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class NccServiceProvider {

  // for mobile
  private url = 'http://simkesma.sasindo.id/api/dinkes';

  // for web
  // private url = '/api/dinkes';

  constructor(private http: Http) {
    console.log('Hello NccServiceProvider Provider');
  }

  /*-----------------------------------
  HALAMAN IBU HAMIL
  -----------------------------------*/

  getBumil(amount:number, page:number) {
    return this.http.get(this.url + '/data-bumil/' + amount + '/' + page)
      .do(this.logResponse)
      .map(this.extractData)
      .do(this.logResponse)
      .catch(this.catchError)
  }

  searchBumil(body) {
    return this.http.post(this.url + '/data-bumil/datatable', body)
      .do(this.logResponse)
      .map(this.extractData)
      .do(this.logResponse)
      .catch(this.catchError)
  }

  inputBumil(body) {
    return this.http.post(this.url + '/data-bumil', body)
      .do(this.logResponse)
      .map(this.extractData)
      .do(this.logResponse)
      .catch(this.catchError);
  }

  editBumil(body) {
    return this.http.post(this.url + '/data-bumil/' + body.get('iddata_bumil'), body)
      .do(this.logResponse)
      .map(this.extractData)
      .do(this.logResponse)
      .catch(this.catchError);
  }

  deleteBumil(iddata_bumil) {
    return this.http.delete(this.url + '/data-bumil/' + iddata_bumil)
      .do(this.logResponse)
      .map(this.extractData)
      .do(this.logResponse)
      .catch(this.catchError);
  }

  /*-----------------------------------
  HALAMAN GIZI BURUK
  -----------------------------------*/

  getGiziburuk(amount:number, page:number) {
    return this.http.get(this.url + '/data-gizi-buruk/' + amount + '/' + page)
      .do(this.logResponse)
      .map(this.extractData)
      .do(this.logResponse)
      .catch(this.catchError)
  }

  getBalita() {
    return this.http.get(this.url + '/data-gizi-buruk/balita')
      .do(this.logResponse)
      .map(this.extractData)
      .do(this.logResponse)
      .catch(this.catchError)
  }

  searchGiziburuk(body) {
    return this.http.post(this.url + '/data-gizi-buruk/datatable', body)
      .do(this.logResponse)
      .map(this.extractData)
      .do(this.logResponse)
      .catch(this.catchError)
  }

  inputBalita(body) {
    return this.http.post(this.url + '/data-gizi-buruk/balita', body)
      .do(this.logResponse)
      .map(this.extractData)
      .do(this.logResponse)
      .catch(this.catchError);
  }

  inputGiziburuk(body) {
    return this.http.post(this.url + '/data-gizi-buruk', body)
      .do(this.logResponse)
      .map(this.extractData)
      .do(this.logResponse)
      .catch(this.catchError);
  }

  editGiziburuk(body) {
    return this.http.post(this.url + '/data-gizi-buruk/' + body.get('iddata_gizi_buruk'), body)
      .do(this.logResponse)
      .map(this.extractData)
      .do(this.logResponse)
      .catch(this.catchError);
  }

  editBalita(body) {
    return this.http.post(this.url + '/data-gizi-buruk/balita/' + body.get('idbalita'), body)
      .do(this.logResponse)
      .map(this.extractData)
      .do(this.logResponse)
      .catch(this.catchError);
  }

  deleteGiziburuk(iddata_gizi_buruk) {
    return this.http.delete(this.url + '/data-gizi-buruk/' + iddata_gizi_buruk)
      .do(this.logResponse)
      .map(this.extractData)
      .do(this.logResponse)
      .catch(this.catchError);
  }

  deleteBalita(iddata_balita) {
    return this.http.delete(this.url + '/data-gizi-buruk/balita/' + iddata_balita)
      .do(this.logResponse)
      .map(this.extractData)
      .do(this.logResponse)
      .catch(this.catchError);
  }

  /*-----------------------------------
  HALAMAN TB
  -----------------------------------*/

  searchTb(body) {
    return this.http.post(this.url + '/data-tb/datatable', body)
      .do(this.logResponse)
      .map(this.extractData)
      .do(this.logResponse)
      .catch(this.catchError)
  }

  inputTb(body) {
    return this.http.post(this.url + '/data-tb', body)
      .do(this.logResponse)
      .map(this.extractData)
      .do(this.logResponse)
      .catch(this.catchError);
  }

  editTb(body) {
    return this.http.post(this.url + '/data-tb/' + body.get('iddata_tb'), body)
      .do(this.logResponse)
      .map(this.extractData)
      .do(this.logResponse)
      .catch(this.catchError);
  }

  deleteTb(iddata_tb) {
    return this.http.delete(this.url + '/data-tb/' + iddata_tb)
      .do(this.logResponse)
      .map(this.extractData)
      .do(this.logResponse)
      .catch(this.catchError);
  }

  getTb(amount:number, page:number) {
    return this.http.get(this.url + '/data-tb/' + amount + '/' + page)
      .do(this.logResponse)
      .map(this.extractData)
      .do(this.logResponse)
      .catch(this.catchError)
  }

  /*-----------------------------------
  RETURN ERROR, RESPONSE & DATA
  -----------------------------------*/

  private catchError(error: Response | any) {
    console.log(error);
    return Observable.throw(error.json().error || "Server error.");
  }

  private logResponse(res: Response) {
    console.log(res);
  }

  private extractData(res: Response) {
    return res.json();
  }

}
